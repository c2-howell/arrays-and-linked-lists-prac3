//
//  Array.h
//  CommandLineTool
//
//  Created by Ciaran Howell on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef H_Array
#define H_Array

#include <iostream>
class Array
{
public:
    Array()
    {
        arraySize=0;
        floatPoint = nullptr;
    }
    ~Array()
    {
        delete[] floatPoint;
    }
    void add (float itemValue)
    {
        //Create new array large enough
        float *floatPointNew = new float[arraySize+1];
        
        //Copy all data from old array to new
        for (int count=0; count < arraySize; count++) {
            floatPointNew[count]=floatPoint[count];
        }
        
        //Add new value at the end
        floatPointNew[arraySize]=itemValue;
        
        //Unless it is a nullptr, delete the old array
        if (floatPoint != nullptr) {
            delete [] floatPoint;
        }
        
        //Point the pointer at new array
        floatPoint = floatPointNew;
        
        //Increase the value of array size
        arraySize++;
    }
    float get (int index)
    {
        return floatPoint[index];
    }
    int size()
    {
        return arraySize;
    }

    
private:
    float *floatPoint=nullptr;
    int arraySize;
};

#endif /* H_Array */
